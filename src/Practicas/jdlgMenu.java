package Practicas;

import java.awt.Dimension;

/**
 *
 * @author LGCD
 */
public class jdlgMenu extends javax.swing.JDialog {

    /**
     * Creates new form jdlgMenu
     */
    public jdlgMenu(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pantallaInicio = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jmiTerreno = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jmiCotizacion = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        jmiRecibo = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jmiSalir = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jmiIMC = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        pantallaInicio.setBackground(new java.awt.Color(102, 102, 255));

        javax.swing.GroupLayout pantallaInicioLayout = new javax.swing.GroupLayout(pantallaInicio);
        pantallaInicio.setLayout(pantallaInicioLayout);
        pantallaInicioLayout.setHorizontalGroup(
            pantallaInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 706, Short.MAX_VALUE)
        );
        pantallaInicioLayout.setVerticalGroup(
            pantallaInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 599, Short.MAX_VALUE)
        );

        jMenu1.setText("Mis Programas:");

        jmiTerreno.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.CTRL_MASK));
        jmiTerreno.setText("Terreno");
        jmiTerreno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiTerrenoActionPerformed(evt);
            }
        });
        jMenu1.add(jmiTerreno);
        jMenu1.add(jSeparator1);

        jmiCotizacion.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        jmiCotizacion.setText("Cotizacion");
        jmiCotizacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiCotizacionActionPerformed(evt);
            }
        });
        jMenu1.add(jmiCotizacion);
        jMenu1.add(jSeparator3);

        jmiRecibo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        jmiRecibo.setText("Recibo");
        jmiRecibo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiReciboActionPerformed(evt);
            }
        });
        jMenu1.add(jmiRecibo);
        jMenu1.add(jSeparator2);

        jmiSalir.setText("Salir");
        jmiSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSalirActionPerformed(evt);
            }
        });
        jMenu1.add(jmiSalir);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Acerca de:");

        jmiIMC.setText("IMC");
        jmiIMC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiIMCActionPerformed(evt);
            }
        });
        jMenu2.add(jmiIMC);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pantallaInicio)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pantallaInicio)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jmiCotizacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiCotizacionActionPerformed
        // TODO add your handling code here:
     
        jifCotizacion co = new jifCotizacion();
        this.pantallaInicio.add(co);
        
        Dimension pantallaSize = pantallaInicio.getSize();
        Dimension FrameSize = co.getSize();
        co.setLocation((pantallaSize.width - FrameSize.width)/2,
        (pantallaSize.height - FrameSize.height)/2);
        
        co.show();
    }//GEN-LAST:event_jmiCotizacionActionPerformed

    private void jmiReciboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiReciboActionPerformed
        // TODO add your handling code here:
       jifRecibo examen = new jifRecibo();
        this.pantallaInicio.add(examen);
        
        Dimension pantallaSize = pantallaInicio.getSize();
        Dimension FrameSize = examen.getSize();
        examen.setLocation((pantallaSize.width - FrameSize.width)/2,
        (pantallaSize.height - FrameSize.height)/2);
        
        examen.show();
    }//GEN-LAST:event_jmiReciboActionPerformed

    private void jmiTerrenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiTerrenoActionPerformed
        // TODO add your handling code here:
        jdlgTerreno ter = new jdlgTerreno(new java.awt.Frame(),true);
        ter.setVisible(true);     
        
        jifTerreno terreno = new jifTerreno();
        this.pantallaInicio.add(ter);
        
        Dimension pantallaSize = pantallaInicio.getSize();
        Dimension frameSize = terreno.getSize();
        terreno.setLocation((pantallaSize.width - frameSize.width)/2,
        (pantallaSize.height - frameSize.height)/2);
        
        terreno.show();
    }//GEN-LAST:event_jmiTerrenoActionPerformed

    private void jmiSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSalirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jmiSalirActionPerformed

    private void jmiIMCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiIMCActionPerformed

    }//GEN-LAST:event_jmiIMCActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jdlgMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jdlgMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jdlgMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jdlgMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jdlgMenu dialog = new jdlgMenu(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JMenuItem jmiCotizacion;
    private javax.swing.JMenuItem jmiIMC;
    private javax.swing.JMenuItem jmiRecibo;
    private javax.swing.JMenuItem jmiSalir;
    private javax.swing.JMenuItem jmiTerreno;
    private javax.swing.JDesktopPane pantallaInicio;
    // End of variables declaration//GEN-END:variables

}
