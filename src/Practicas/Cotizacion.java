package Practicas;

/**
 *
 * @author LGCD
 */
public class Cotizacion {
 //Atributos de la clase
    private String folio;
    private String descripcion;
    private float precio;
    private float porcentajeInicial;
    private int plazo;
    private float pagoInicial;
    private int total;
    
    public Cotizacion(){
    this.folio="";
      this.descripcion="";
      this.precio=0.0f;
      this.porcentajeInicial=0.0f;
      this.plazo=0;
      this.pagoInicial=0.0f;
      this.total=0;
    
    }

    //Constructor por argumentos
    public Cotizacion(int numCotizacion, String descripcion, float precio, float porcentajeInicial, int plazo, float pagoInicial, int total) {
        this.folio = folio;
        this.descripcion = descripcion;
        this.precio = precio;
        this.porcentajeInicial = porcentajeInicial;
        this.plazo = plazo;
        this.pagoInicial = pagoInicial;
        this.total = total;
    }
    
    //Copia

    public Cotizacion(Cotizacion otro) {
        this.folio = otro.folio;
        this.descripcion = otro.descripcion;
        this.precio = otro.precio;
        this.porcentajeInicial = otro.porcentajeInicial;
        this.plazo = otro.plazo;
        this.pagoInicial = otro.pagoInicial;
        this.total = otro.total;
    }
    
    //Metodos Set y Get

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajeInicial() {
        return porcentajeInicial;
    }

    public void setPorcentajeInicial(float porcentajeInicial) {
        this.porcentajeInicial = porcentajeInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public float getPagoInicial() {
        return pagoInicial;
    }

    public void setPagoInicial(float pagoInicial) {
        this.pagoInicial = pagoInicial;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
    
    //Metodos de comportamient
     //Metodos de comportamiento
    public float calcularPagoIn(){
    float calcularPagoIn=0.0f;
    calcularPagoIn=this.precio*this.porcentajeInicial;
    return calcularPagoIn;
    }
    public float calcularTotalFin(){
    float calcularTotalFin=0.0f;
    calcularTotalFin=this.precio-this.calcularPagoIn();
    return calcularTotalFin;
    }
    
    public float calcularPagoMen(){
    float calcularPagoMen=0.0f;
    calcularPagoMen=this.calcularTotalFin()/this.plazo;
    return calcularPagoMen;
    }


    }
    

