package Practicas;

/**
 *
 * @author LGCD
 */
public class Recibo {
    //Atributos de la clase
    private int id;
    private int numRecibo;
    private String nombre;
    private String domicilio;
    private String fecha;
    private int servicio;
    private float kilowatts;
    private float kilowattsConsumidos;
    private int status;
    
    public Recibo(){
    this.id=0;
    this.numRecibo=0;
    this.nombre="";
    this.domicilio="";
    this.fecha="";
    this.servicio=0;
    this.kilowatts=0.0f;
    this.kilowattsConsumidos=0.0f;
    this.status=0;
    }

    //Constructor por argumentos
    public Recibo(int id, int numRecibo, String nombre, String domicilio, String fecha, int servicio, float kilowatts, float kilowattsConsumidos, int status) {
        this.id = id;
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.fecha = fecha;
        this.servicio = servicio;
        this.kilowatts = kilowatts;
        this.kilowattsConsumidos = kilowattsConsumidos;
        this.status = status;
    }

    //Copia
    public Recibo(Recibo otro) {
        this.id = otro.id;
        this.numRecibo = otro.numRecibo;
        this.nombre = otro.nombre;
        this.domicilio = otro.domicilio;
        this.fecha = otro.fecha;
        this.servicio = otro.servicio;
        this.kilowatts = otro.kilowatts;
        this.kilowattsConsumidos = otro.kilowattsConsumidos;
        this.status = otro.status;
    }
    
    //Metodos Set y Get
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getServicio() {
        return servicio;
    }

    public void setServicio(int servicio) {
        this.servicio = servicio;
    }

    public float getKilowatts() {
        return kilowatts;
    }

    public void setKilowatts(float kilowatts) {
        this.kilowatts = kilowatts;
    }

    public float getKilowattsConsumidos() {
        return kilowattsConsumidos;
    }

    public void setKilowattsConsumidos(float kilowattsConsumidos) {
        this.kilowattsConsumidos = kilowattsConsumidos;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
    //Metodos de comportamiento
    public float calcularSubTotal(){
    float subTotal=0.0f;
    if(servicio==1){
    subTotal=this.kilowattsConsumidos*2.00f;
    }
    if(servicio==2){
    subTotal=this.kilowattsConsumidos*3.00f;
    }
    if(servicio==3){
    subTotal=this.kilowattsConsumidos*5.00f;
    }
        return subTotal;
    }
    
    public float calcularImpuesto(){
    float impuesto=0.0f;
    impuesto=this.calcularSubTotal()*0.16f;
    return impuesto;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularSubTotal()+this.calcularImpuesto();
    return total;
    }

    boolean isSelected() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}


