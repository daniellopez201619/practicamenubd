package Practicas;
/**
 *
 * @author LGCD
 */
public class TestRecibo {

    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Verificando errores");
        dbRecibo rec = new dbRecibo();
        rec.conectar();
        
        Recibo recibo = new Recibo();
        recibo.setNumRecibo(100);
        recibo.setNombre("Jose Miguel ");
        recibo.setDomicilio("Av. Sol 33");
        recibo.setKilowatts(10000.0f);
        recibo.setKilowattsConsumidos(1211f);
        recibo.setServicio(1);
        recibo.setFecha("2023-12-08");
        
        //rec.insertar(recibo);
        rec.actualizar(recibo);
        
        rec.deshabilitar(recibo);
        if(rec.isExiste(100,0)) System.out.println("Si existe");
        else System.out.println("No existe");
        rec.habilitar(recibo);
        if(rec.isExiste(100,0))System.out.println("Si existe");
        else System.out.println("No existe");
        
        Recibo result= rec.buscar(100);
        
        if(result.getId()>0 ) {
        
            System.out.println(result.getNombre() +"dom" +result.getDomicilio());
        
        }
    }
    
}
